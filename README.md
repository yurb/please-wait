A musical composition with an browser-based visual accompaniment.

Content of this repository is licensed under [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) License.
