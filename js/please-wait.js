$(function () {
    "use strict";

    /* DOM */
    var ui = $('#ui');
    var soundtrack = $('#soundtrack').get(0);
   
    /* The percentage indicator will update only occasionally, to make
     * it more intriguing. We'll generate an array of percentages to
     * report, all the numbers between them will be skiped by the
     * indicator. */
    var pcts = Array();
    for (var i = 0; i < 14; i++) {
        var num = Math.floor(Math.random() * 100);
        pcts.push(num);
    }

    var initialized = false;
    $(soundtrack).on('canplaythrough', function () {
        if (! initialized) {
            ui.find('.loading').fadeOut('slow', function () {
                ui.find('#start').fadeIn('slow');
            });
            initialized = true;
        }
    });

    var progress = function () {
        var percentage;
        if (soundtrack.currentTime == 0) {
            percentage = 0;
        } else {
            percentage = soundtrack.currentTime / soundtrack.duration;
            percentage = Math.floor(percentage * 100);
        }

        if (percentage == 0 ||
            percentage == 100 ||
            pcts.indexOf(percentage) != -1) {
            ui.find('.percentage').text(percentage + '%');
        }

        if (percentage < 100) {
            setTimeout(progress, 1000);
        } else {
            ui.find('.please-wait').fadeOut('slow', function () {
                $(this).text('(the end)').fadeIn('slow');
            });
            ui.find('.credits').show('slow');
        }
    }

    ui.find('#start a').on('click', function (ev) {
        ev.preventDefault();
        $(this).fadeOut('fast', function () {
            ui.find('#progress').fadeIn('slow');
            soundtrack.play();
            progress();
        });
    });
});
